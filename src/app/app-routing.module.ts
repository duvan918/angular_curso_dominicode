import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { YedufroComponent } from './pages/yedufro/yedufro.componwnt';

const routes: Routes = [
  {path: 'yedufro', component: YedufroComponent},
  { path: 'products', loadChildren: () => import('./pages/products/products.module').then(m => m.ProductsModule) },
  {path: '**', redirectTo: '', pathMatch:'full'}  // Dejar siempre al final de las rutas
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
