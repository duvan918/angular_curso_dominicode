import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from '../interfaces/product.interface';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() product!: Product; //De padre a hijo
  @Output() addToCartClick = new EventEmitter<Product>(); //De hijo a padre
  constructor() { }

  ngOnInit(): void {
  }

  onClick(): void {
    // console.log(this.product);
    this.addToCartClick.emit(this.product);
  }

}
